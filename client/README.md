# NotificationFcm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


# How It's Work

 this is refference Part 1 and part 2
 https://dev.to/amalja0/how-to-integrating-firebase-cloud-messaging-angular-expressjs-2-create-and-integrate-angular-with-firebase-2f1e 
 check browser and device settings allow notification 

create firebase new project
npm install Services Worker and firebase in angular project
copy sender_id from settings cloud messaging tab and add that key into manifest.json
copy firebaseConfig settings from General settings and add that config into environements.ts file
add firebase-messaging-sw.js services worker in project
add   "src/manifest.json", "src/firebase-messaging-sw.js" in assets into angular.json file
write requestPermission() function in app.component.ts
register your service worker in index.html in script tag into body
run project and click on send notification

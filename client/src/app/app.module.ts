import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { envirenement } from '../environements/environement';
import { initializeApp } from 'firebase/app';
import { messaging } from "../configs/firebase.config";
initializeApp(envirenement.firebase);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    { provide: 'messaging', useValue: messaging }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

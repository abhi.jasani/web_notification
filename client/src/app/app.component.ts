import { Component, OnInit } from '@angular/core';
import { getMessaging, getToken } from 'firebase/messaging';
import { envirenement } from '../environements/environement';
import { messaging } from '../configs/firebase.config';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {

  abc = "";
  ngOnInit(): void {
    this.requestPermission();
    this.listen();
  }
  requestPermission() {
    messaging.getToken({ vapidKey: envirenement.firebase.vpaidKey })
      .then((currentToken) => {
        if (currentToken) {
          this.abc = currentToken;
          console.log(currentToken);
        } else {
          console.log('No registration token available. Request permission to generate one.');
        }
      }).catch((err) => {
        console.log(err);
      });
  }

  listen() {
    messaging.onMessage((incomingMessage) => {
      console.log(incomingMessage);
    })
  }

}

import firebase from 'firebase/compat/app';
import { envirenement } from "../environements/environement";
import 'firebase/compat/messaging';

firebase.initializeApp(envirenement.firebase);
export const messaging = firebase.messaging();
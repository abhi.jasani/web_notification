using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Services.notification;

[ApiController]
[Route("api/[controller]")]
public class FcmController : ControllerBase
{
    private readonly NotificationService _fcmApiService;

    public FcmController()
    {
        _fcmApiService = new NotificationService();
    }

    [HttpPost("send-notification")]
    public async Task<IActionResult> SendNotification([FromBody] FcmNotificationRequest request)
    {
        try
        {
            string token = await _fcmApiService.SendFcmNotificationAsync(request.Title, request.Body, request.Token);
            return Ok(new { Message = "FCM notification sent successfully.", token });
        }
        catch
        {
            return BadRequest(new { Error = "Failed to send FCM notification." });
        }
    }
}

public class FcmNotificationRequest
{
    public string Title { get; set; }
    public string Body { get; set; }
    public string Token { get; set; }
}

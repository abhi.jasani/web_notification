using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;

namespace Services.notification;
public class NotificationService
{
    private readonly HttpClient _httpClient;
    string fcmUrl = "https://fcm.googleapis.com/v1/projects/web-nofitication/messages:send";
    private const string ServerKey = "AAAAUzV3AKA:APA91bHaxoWG4jnL6jKpZHhuYAAMDAXG-skBHqvrxKertsOk_RQgoBNugvaP27udboIwb1d5pLRGSRxH9OGASvi60hWgsYQDeh8-XM6ULu-6PMFY9AD4cAT6j4VxXwpIynpGIftgMHxA"; // Replace with your actual server key

    public NotificationService()
    {
        _httpClient = new HttpClient();
    }

    public async Task<string> SendFcmNotificationAsync(string title, string body, string token)
    {
        try
        {

            string fileName = "./web-nofitication-firebase-adminsdk-sxnof-92d5243f2e.json"; //Download from Firebase Console ServiceAccount

            string scopes = "https://www.googleapis.com/auth/firebase.messaging";
            string bearertoken = ""; // Bearer Token in this variable

            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                bearertoken = GoogleCredential
                    .FromStream(stream) // Loads key file
                    .CreateScoped(scopes) // Gathers scopes requested
                    .UnderlyingCredential // Gets the credentials
                    .GetAccessTokenForRequestAsync().Result; // Gets the Access Token
            }
            Console.WriteLine(bearertoken);

            string payload = @"{
                ""message"": {
                    ""notification"": {
                        ""title"": """ + title + @""",
                        ""body"": """ + body + @"""
                    },
                    ""token"": """ + token + @"""
                }
            }";

            _httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {bearertoken}");
            var content = new StringContent(payload, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(fcmUrl, content);

            response.EnsureSuccessStatusCode();
            return bearertoken;
        }
        catch (Exception ex)
        {
            // Handle exception as needed
            Console.WriteLine($"Error sending FCM notification: {ex.Message}");
            return "";
        }
    }
}